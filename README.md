## Found problems

1. Wrong formula to calculate distance (not all values were casted to radians)
1. Mistake in calculation of radius statistics (RestWeatherQueryEndpoint.java:L89) - used `%` instead of `/`
1. Mistake in calculation of freq_iata in statistics. I guess number of requests for IATA items supposed to be divided 
  by total number of requests rather than number of requested IATA items.
1. No documentation for some not obvious methods, including calculation of statistics. 
  Hope that I've understood correctly.
1. Everything in single package.
1. All logic in single classes, classes became too complicated, difficult to test.
1. Statistics: not optimal data structures, not synchronous maps.
1. No persistence
1. Not enough tests (unit tests would have found mistakes in #1 and #2)
1. On adding of new airport there was `ad.setLatitude(longitude);`. Obviously an error.

#### small problems, imperfections

1. Unnecessary library Gson. Jackson already included with `jersey-media-json-jackson`.
2. In RestWeatherQueryEndpoint one method returns String, another returns Response. I didn't change it as it would 
 require change in DoNotChangeTest.
3. I didn't understand if I should do anything WeatherClient and left it as it was (practically).
 

## What have done

1. Fixed formulas.
2. Splitted logic of some complex methods to multiple classes and methods. 
3. Added some documentation.
4. Added some tests. Even though the final coverage is only 60%, the most of uncovered code is in config/client/server classes.
5. Dependency injection.
6. Refactoring statistics (no persistence for it as it was said in documentation for one of the structures).
7. AirportLoader

## What could be done

1. Persistence? Not sure that it's needed because that could have some side effects. 
  For example for one of the request we retrieve all airports information. Calculation of the distance on db side or storing 
  it in the db would be good but that would cost a lot of time. 
  In assignment.md it's said that application should suppport 1000 of airports. 1000 of airports can live in memory without problems. 

1. If finding of airports in certain radius works too long, distance between airports could be stored in the db (or in "InMemoryRepository", doesn't matter).
  But it looks like too complicated task to do it now (I don't have enough time, I'd prefer to see statistics of real usage).
  Another solution: use a simplier formula to find approximate distance, get candidates, then to use full formula to that candidates.
  