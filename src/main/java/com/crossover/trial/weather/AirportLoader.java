package com.crossover.trial.weather;

import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.DST;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple airport loader which reads a file from disk and sends entries to the webservice
 * <p>
 * File structure is defined in assignment.md - #Add a new feature part
 * <p>
 * <b>
 * FIXME file description in assignment.md doesn't contain 2 first fields that were in template airports.dat. Quite misleading.
 * I hope that file that will be used for evaluation will be similar to template file.
 * </b>
 *
 * @author code test administrator
 */
@Slf4j
public class AirportLoader {

    private static final String[] FILE_HEADERS = {
            "Number",
            "Name",
            "City",
            "Country",
            "IATA",
            "ICAO",
            "Latitude",
            "Longitude",
            "Altitude",
            "Timezone",
            "DST"
    };
    private static final String DEFAULT_HOST = "http://localhost:9090/";
    private static final String USAGE =
            "filename [-host host]" + System.lineSeparator() +
                    "    filename - airports.dat file" + System.lineSeparator() +
                    "    host - optional paramater for server host. Default - '" + DEFAULT_HOST + "'";

    /**
     * end point to supply updates
     */
    private WebTarget collect;

    public AirportLoader(URI host) throws URISyntaxException {
        Client client = ClientBuilder.newClient();
        collect = client.target(host).path("collect/airport");
    }

    public void upload(Reader reader) throws IOException {
        List<AirportData> dataList = parseAirportData(reader);
        dataList.forEach(airport -> {
            try {
                upload(airport);
            } catch (IOException e) {
                log.error("error occured uploading {}", airport, e);
            }
        });
    }

    public void upload(AirportData data) throws IOException {
        Response response = collect.request().post(Entity.entity(data, MediaType.APPLICATION_JSON_TYPE));
        switch (response.getStatusInfo().getFamily()) {
            case SUCCESSFUL:
                log.info("successfully uploaded {} - {}", data.getIata(), data.getName());
                break;
            case CLIENT_ERROR:
            case SERVER_ERROR:
                log.warn("error on {} - {}: status {}, message {}", data.getIata(), data.getName(),
                        response.getStatus(), String.valueOf(response.getEntity()));
                break;
            default:
                log.warn("unexpected result on {} - {}: status {}, message {}", data.getIata(), data.getName(),
                        response.getStatus(), String.valueOf(response.getEntity()));
        }
    }

    static List<AirportData> parseAirportData(Reader reader) throws IOException {
        List<AirportData> dataList = new ArrayList<>();
        CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader(FILE_HEADERS));
        for (CSVRecord record : parser) {
            try {
                dataList.add(createAirportData(record));
            } catch (Exception e) {
                log.warn("error parsing " + record, e);
            }
        }
        return dataList;
    }


    static AirportData createAirportData(CSVRecord record) {
        final AirportData data = new AirportData();
        //Name
        data.setName(record.get("Name"));
        //City
        data.setCity(record.get("City"));
        //Country
        data.setCountry(record.get("Country"));
        //IATA
        data.setIata(record.get("IATA"));
        //ICAO
        data.setIcao(record.get("ICAO"));
        //Latitude
        data.setLatitude(Double.parseDouble(record.get("Latitude")));
        //Longitude
        data.setLongitude(Double.parseDouble(record.get("Longitude")));
        //Atitude
        data.setAltitude(Double.parseDouble(record.get("Altitude")));
        //Timezone
        data.setTimezone(zoneOffsetFromStringOffset(record.get("Timezone")));
        //DST
        data.setDst(DST.valueOf(record.get("DST")));

        return data;
    }

    static ZoneOffset zoneOffsetFromStringOffset(String offset) {
        final BigDecimal sixty = BigDecimal.valueOf(60);

        BigDecimal bigDecimal = new BigDecimal(offset);
        final int hours = bigDecimal.intValue();
        bigDecimal = bigDecimal.remainder(BigDecimal.ONE).multiply(sixty);
        final int minutes = bigDecimal.intValue();
        final int seconds = bigDecimal.remainder(BigDecimal.ONE).multiply(sixty).intValue();
        return ZoneOffset.ofHoursMinutesSeconds(hours, minutes, seconds);
    }


    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            wrongUsage();
        }
        final File airportsDataFile = new File(args[0]);
        if (!airportsDataFile.exists() || !airportsDataFile.isFile() || airportsDataFile.length() == 0) {
            System.err.println(airportsDataFile + " is not a valid input");
            System.exit(1);
        }

        String host = DEFAULT_HOST;
        if (args.length > 1) {
            for (int i = 1; i < args.length; i++) {
                try {
                    switch (args[i]) {
                        case "-host":
                            host = args[++i];
                            break;
                        default:
                            wrongUsage();
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    wrongUsage();
                }
            }
        }

        AirportLoader al = new AirportLoader(new URI(host));
        try (FileReader reader = new FileReader(airportsDataFile)) {
            al.upload(reader);
        }
    }

    private static void wrongUsage() {
        System.err.println(USAGE);
        System.exit(1);
    }
}
