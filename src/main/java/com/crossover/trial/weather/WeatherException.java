package com.crossover.trial.weather;

/**
 * An internal exception marker
 */
public class WeatherException extends RuntimeException {
    public WeatherException() {
        super();
    }

    public WeatherException(String message) {
        super(message);
    }
}
