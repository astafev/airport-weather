package com.crossover.trial.weather.api;

import com.crossover.trial.weather.WeatherException;
import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.domain.DataPointType;
import com.crossover.trial.weather.service.CollectorService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.grizzly.http.server.HttpServer;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static com.crossover.trial.weather.config.ExceptionLoggerMapper.errorEntity;

/**
 * A REST implementation of the WeatherCollector API. Accessible only to airport weather collection
 * sites via secure VPN.
 *
 * @author code test administrator
 */

@Path("/collect")
@Slf4j
public class RestWeatherCollectorEndpoint implements WeatherCollectorEndpoint {

    @Inject
    CollectorService service;

    private static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Response ping() {
        return Response.status(Response.Status.OK).entity("ready").build();
    }

    @POST
    @Path("/weather/{iata}/{pointType}")
    @Override
    public Response updateWeather(@PathParam("iata") String iataCode,
                                  @PathParam("pointType") String pointType,
                                  String datapointJson) {
        try {
            service.addDataPoint(iataCode,
                    DataPointType.valueOf(pointType.toUpperCase()),
                    objectMapper.readValue(datapointJson, DataPoint.class));
            return Response.status(Response.Status.OK).build();
        } catch (IllegalArgumentException | JsonMappingException | JsonParseException e) {
            log.warn("user provided wrong input", e);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(errorEntity(e.getMessage())).build();
        } catch (WeatherException | IOException e) {
            log.error("exception on update weather with {} iata code", iataCode, e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @Override
    public Response getAirports() {
        return Response.status(Response.Status.OK).entity(service.getAirportsIataCodes()).build();
    }


    @Override
    public Response getAirport(@PathParam("iata") String iata) {
        AirportData ad = service.getAirportData(iata);
        if (ad == null) {
            return Response.status(404).build();
        } else {
            return Response.status(Response.Status.OK).entity(ad).build();
        }
    }

    @POST
    @Path("/airport/{iata}/{lat}/{long}")
    @Override
    public Response addAirport(@PathParam("iata") String iata,
                               @PathParam("lat") String latString,
                               @PathParam("long") String longString) {
        try {
            service.addAirport(iata, Double.valueOf(latString), Double.valueOf(longString));
            return Response.status(Response.Status.OK).build();
        } catch (NumberFormatException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(errorEntity(e.getMessage())).build();
        }
    }

    @Override
    public Response addAirport(String message) throws IOException {
        try {
            AirportData airportData = objectMapper.readValue(message, AirportData.class);
            if (airportData.getIata() == null || airportData.getLongitude() == null
                    || airportData.getLatitude() == null) {
                throw new JsonMappingException("one of mandatory parameters wasn't specified: either IATA, " +
                        "or latitude, or longitude");
            }
            service.addAirport(airportData);
            //iata code serves as url, so no need to return anything to the client
            return Response.ok().build();
        } catch (JsonMappingException | JsonParseException e) {
            log.warn("user provided wrong input '{}'", message, e);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(errorEntity(e.getMessage())).build();
        }
    }

    @DELETE
    @Path("/airport/{iata}")
    @Override
    public Response deleteAirport(@PathParam("iata") String iata) {
        final AirportData airportData = service.deleteAirport(iata);
        if (airportData == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok().build();
        }
    }

    @Override
    public Response exit() {
        /* this method can't be changed without serious change in WeatherServer class. So let's keep it like that.
        * anyway this method doesn't look like it should be available to public in real applicaiton */
        System.exit(0);
        return Response.noContent().build();
    }
}
