package com.crossover.trial.weather.api;

import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.service.QueryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.crossover.trial.weather.config.ExceptionLoggerMapper.errorEntity;


/**
 * The Weather App REST endpoint allows clients to query, update and check health stats. Currently, all data is
 * held in memory. The end point deploys to a single container
 *
 * @author code test administrator
 */
@Path("/query")
@Slf4j
public class RestWeatherQueryEndpointImpl implements WeatherQueryEndpoint {

    /**
     * shared gson json to object factory
     */

    @Inject
    QueryService queryService;

    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Retrieve service health including total size of valid data points and request frequency information.
     *
     * @return health stats for the service as a string
     */
    @Override
    public String ping() {
        try {
            return objectMapper.writeValueAsString(queryService.ping());
        } catch (JsonProcessingException e) {
            //shouldn't happen
            throw new IllegalStateException(e);
        }
    }

    /**
     * Given a query in json format {'iata': CODE, 'radius': km} extracts the requested airport information and
     * return a list of matching atmosphere information.
     *
     * @param iata         the iataCode
     * @param radiusString the radius in km
     * @return a list of atmospheric information
     */
    @Override
    public Response weather(String iata, String radiusString) {
        try{
            double radius = StringUtils.isBlank(radiusString) ? 0 : Double.parseDouble(radiusString);
            final List<AtmosphericInformation> weather = queryService.weather(iata, radius);
            return Response.status(Response.Status.OK).entity(weather).build();
        } catch (IllegalArgumentException e) {
            log.warn("user provided wrong input", e);
            return Response.status(Response.Status.BAD_REQUEST).entity(errorEntity(e.getMessage())).build();
        }
    }


}
