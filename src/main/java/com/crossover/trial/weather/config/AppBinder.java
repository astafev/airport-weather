package com.crossover.trial.weather.config;

import com.crossover.trial.weather.repository.AirportRepository;
import com.crossover.trial.weather.repository.AtmosphericInformationRepository;
import com.crossover.trial.weather.repository.InMemoryRepository;
import com.crossover.trial.weather.service.*;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;

/**
 * Required to enable and configure dependency injection.
 * */
public class AppBinder extends AbstractBinder {
    @Override
    protected void configure() {
        //services
        bind(CollectorServiceImpl.class).to(CollectorService.class).in(Singleton.class);
        bind(QueryServiceImpl.class).to(QueryService.class).in(Singleton.class);
        bind(StatisticsServiceImpl.class).to(StatisticsService.class).in(Singleton.class);
        //repositories
        bind(InMemoryRepository.class).to(AtmosphericInformationRepository.class)
                .to(AirportRepository.class)
                .in(Singleton.class);
    }
}
