package com.crossover.trial.weather.config;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Collections;

@Slf4j
public class ExceptionLoggerMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable ex) {
        log.error("", ex);

        return Response.status(500)
                .entity(errorEntity(ex.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    public static Object errorEntity(String message) {
        return Collections.singletonMap("errorMessage", message);
    }
}
