package com.crossover.trial.weather.domain;

import com.crossover.trial.weather.util.ZoneOffsetSerializationUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.ZoneOffset;

/**
 * Basic airport information.
 *
 * @author code test administrator
 */
@Data
@EqualsAndHashCode(of = "iata")
@NoArgsConstructor
public class AirportData {

    /**
     * the three letter IATA code
     */
    private String iata;

    private String name;

    /**
     * Main city served by airport. May be spelled differently from name.
     */
    private String city;

    /**
     * Country or territory where airport is located.
     */
    private String country;

    /**
     * 4-letter ICAO code (blank or "" if not assigned)
     */
    private String icao;

    /**
     * latitude value in degrees
     */
    private Double latitude;

    /**
     * longitude value in degrees
     */
    private Double longitude;

    /**
     * Altitude in feet
     */
    private Double altitude;

    /**
     * Hours offset from UTC.
     */
    @JsonSerialize(using = ZoneOffsetSerializationUtils.ZoneOffsetSerializer.class)
    @JsonDeserialize(using = ZoneOffsetSerializationUtils.ZoneOffsetDeserializer.class)
    private ZoneOffset timezone;

    /**
     * One of E (Europe), A (US/Canada), S (South America), O (Australia), Z (New Zealand), N (None) or U (Unknown)
     */
    private DST dst;

    private AtmosphericInformation information = new AtmosphericInformation();

    /**
     * used in init
     */
    public AirportData(String iata, double latitude, double longitude) {
        this.iata = iata;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
