package com.crossover.trial.weather.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.Instant;

/**
 * encapsulates sensor information for a particular location
 */
@Data
public class AtmosphericInformation {

    /**
     * temperature in degrees celsius
     */
    private DataPoint temperature;

    /**
     * wind speed in km/h
     */
    private DataPoint wind;

    /**
     * humidity in percent
     */
    private DataPoint humidity;

    /**
     * precipitation in cm
     */
    private DataPoint precipitation;

    /**
     * pressure in mmHg
     */
    private DataPoint pressure;

    /**
     * cloud cover percent from 0 - 100 (integer)
     */
    private DataPoint cloudCover;

    /**
     * the last time this data was updated, in milliseconds since UTC epoch.
     * <p>
     * had protected getter in initial implementation, to keep external interface the same, we'll add @JsonIgnore
     */
    @JsonIgnore
    private Instant lastUpdateTime = Instant.now();

    /**
     * Checks if object has any data.
     *
     * @return true if one of fields is set, false otherwise
     */
    @JsonIgnore
    public boolean isNotEmpty() {
        return temperature != null || wind != null
                || humidity != null || precipitation != null
                || pressure != null || cloudCover != null;
    }
}
