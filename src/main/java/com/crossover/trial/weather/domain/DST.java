package com.crossover.trial.weather.domain;

public enum DST {
    E("Europe"),
    A("US/Canada"),
    S("South America"),
    O("Australia"),
    Z("New Zealand"),
    N("None"),
    U("Unknown");

    private final String fullName;

    DST(String fullName) {
        this.fullName = fullName;
    }
}
