package com.crossover.trial.weather.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * A collected point, including some information about the range of collected values
 *
 * @author code test administrator
 */
@Data
@Builder
// for builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
// for jackson
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DataPoint {

    /** the mean of the observations */
    private double mean;

    /** 1st quartile -- useful as a lower bound */
    private int first;

    /** 2nd quartile -- median value */
    @JsonProperty("second")
    private int median;

    /** 3rd quartile value -- less noisy upper value */
    @JsonProperty("third")
    private int last;

    /** the total number of measurements */
    private int count;

}
