package com.crossover.trial.weather.domain;

/**
 * The various types of data points we can collect.
 *
 * @author code test administrator
 */
public enum DataPointType {
    WIND(0, Double.POSITIVE_INFINITY),
    TEMPERATURE(-50, 100),
    HUMIDITY(0, 100),
    PRESSURE(650, 800),
    CLOUDCOVER(0, 100),
    PRECIPITATION(0, 100);

    final double border1;
    final double border2;

    DataPointType(double border1, double border2) {
        this.border1 = border1;
        this.border2 = border2;
    }

    /**
     * checks if parameter is
     * */
    public boolean insideBorders(double mean) {
        return mean >= border1 && mean < border2;
    }
}
