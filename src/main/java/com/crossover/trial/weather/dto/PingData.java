package com.crossover.trial.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Map;

@Data
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class PingData {
    private final Map<String, Double> iataFreq;
    private final int[] radiusFreq;
    private final long datasize;

    public PingData(@JsonProperty("datasize") long datasize,
                    @JsonProperty("iata_freq") Map<String, Double> iataFreq,
                    @JsonProperty("radius_freq") int[] radiusFreq) {
        this.datasize = datasize;
        this.iataFreq = iataFreq;
        this.radiusFreq = radiusFreq;
    }
}
