package com.crossover.trial.weather.repository;

import com.crossover.trial.weather.domain.AirportData;

import java.util.List;
import java.util.Set;

public interface AirportRepository {

    AirportData findById(String iata);

    AirportData addAirport(AirportData data);

    List<AirportData> findAll();

    Set<String> listAllIataCodes();

    AirportData delete(String iata);
}
