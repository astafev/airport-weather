package com.crossover.trial.weather.repository;

import java.time.Instant;

public interface AtmosphericInformationRepository {

    /**
     * Counts all saved atmospheric informations which have at least one non null property and where lastUpdated
     * is greater than provided param. Used in statistics request.
     */
    long countAllNonEmptyWhereLastUpdateGreater(Instant lastUpdate);
}
