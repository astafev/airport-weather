package com.crossover.trial.weather.repository;

import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.AtmosphericInformation;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementation based on initial version - just saving everything to a list/map
 * <p>
 * Not sure that persistence is needed in the project.
 * For example for one of the request we retrieve all airports information. Calculation of the distance on db side or storing
 * it in the db would be good but that would require a lot of time to implement it. I suppose it's out of scope of this task.
 * <p>
 * In assignment.md it's said that application should suppport 1000 of airports.
 * 1000 of airports can live in memory without problems.
 */
@Slf4j
public class InMemoryRepository implements AirportRepository, AtmosphericInformationRepository {
    private final Map<String, AirportData> airports = new ConcurrentHashMap<>();

    public InMemoryRepository() {
//        init();
    }

    @Override
    public AirportData findById(String iata) {
        return airports.get(iata);
    }

    @Override
    public AirportData addAirport(AirportData data) {
        airports.put(data.getIata(), data);
        return data;
    }

    @Override
    public List<AirportData> findAll() {
        return new ArrayList<>(airports.values());
    }

    @Override
    public Set<String> listAllIataCodes() {
        return airports.keySet();
    }

    @Override
    public AirportData delete(String iata) {
        return airports.remove(iata);
    }

    @Override
    public long countAllNonEmptyWhereLastUpdateGreater(Instant lastUpdate) {
        return airports.values().stream().map(AirportData::getInformation)
                .filter(information -> information != null)
                .filter(AtmosphericInformation::isNotEmpty)
                .filter(info -> info.getLastUpdateTime().isAfter(lastUpdate))
                .count();
    }


    /**
     * A dummy init method that loads hard coded data
     */
    private void init() {
        airports.clear();

        addAirport(new AirportData("BOS", 42.364347, -71.005181));
        addAirport(new AirportData("EWR", 40.6925, -74.168667));
        addAirport(new AirportData("JFK", 40.639751, -73.778925));
        addAirport(new AirportData("LGA", 40.777245, -73.872608));
        addAirport(new AirportData("MMU", 40.79935, -74.4148747));
    }
}
