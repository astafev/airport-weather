package com.crossover.trial.weather.service;

import com.crossover.trial.weather.WeatherException;
import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.domain.DataPointType;

import java.util.Set;

public interface CollectorService {
    /**
     * Update the airports weather data with the collected data.
     *
     * @param iataCode  the 3 letter IATA code
     * @param pointType the point type {@link DataPointType}
     * @param dataPoint a datapoint object holding pointType data
     * @throws WeatherException if the update can not be completed
     */
    void addDataPoint(String iataCode, DataPointType pointType, DataPoint dataPoint) throws WeatherException;

    /**
     * Add a new airport to the known airport list.
     *
     * @param iataCode  3 letter code
     * @param latitude  in degrees
     * @param longitude in degrees
     * @return the added airport
     */
    AirportData addAirport(String iataCode, double latitude, double longitude);

    /**
     * Adds a new airport. Different from {@link #addAirport(String, double, double)}, can carry more data.
     * */
    AirportData addAirport(AirportData airportData);

    /**
     * Removes airport with given iataCode.
     * <p>
     * Statistics information about this airport is kept.
     *
     * @return deleted airport of `null` if there was no airport with given iataCode
     */
    AirportData deleteAirport(String iataCode);

    /**
     * Retrieves information about all airports
     */
    Set<String> getAirportsIataCodes();

    /**
     * Retrieves information about certain airport
     */
    AirportData getAirportData(String iata);
}
