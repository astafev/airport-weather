package com.crossover.trial.weather.service;

import com.crossover.trial.weather.WeatherException;
import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.domain.DataPointType;
import com.crossover.trial.weather.repository.AirportRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Set;

@Slf4j
public class CollectorServiceImpl implements CollectorService {

    @Inject
    private AirportRepository repository;

    @Override
    public void addDataPoint(String iataCode, DataPointType pointType, DataPoint dp) throws WeatherException {
        final AirportData airport = repository.findById(iataCode);
        AtmosphericInformation information = airport.getInformation();
        updateAtmosphericInformation(information, pointType, dp);
    }

    /**
     * update atmospheric information with the given data point for the given point type
     *
     * @param ai     the atmospheric information object to update
     * @param dptype the data point type as a string
     * @param dp     the actual data point
     */
    void updateAtmosphericInformation(AtmosphericInformation ai, DataPointType dptype, DataPoint dp) throws WeatherException {
        if (dptype.insideBorders(dp.getMean())) {
            switch (dptype) {
                case WIND:
                    ai.setWind(dp);
                    break;
                case TEMPERATURE:
                    ai.setTemperature(dp);
                    break;
                case HUMIDITY:
                    ai.setHumidity(dp);
                    break;
                case PRESSURE:
                    ai.setPressure(dp);
                    break;
                case CLOUDCOVER:
                    ai.setCloudCover(dp);
                    break;
                case PRECIPITATION:
                    ai.setPrecipitation(dp);
                    break;
                default:
                    throw new IllegalStateException("couldn't update atmospheric data, unknown dbtype: " + dptype.name());
            }
            ai.setLastUpdateTime(Instant.now());
        } else {
            log.warn("value {} is not allowed for {}", dp.getMean(), dptype.name());
        }
    }


    @Override
    public AirportData addAirport(String iataCode, double latitude, double longitude) {
        AirportData ad = new AirportData();

        ad.setIata(iataCode);
        ad.setLatitude(latitude);
        ad.setLongitude(longitude);

        ad.setInformation(new AtmosphericInformation());

        return repository.addAirport(ad);
    }

    @Override
    public AirportData addAirport(AirportData airportData) {
        return repository.addAirport(airportData);
    }

    @Override
    public AirportData deleteAirport(String iataCode) {
        return repository.delete(iataCode);
    }


    @Override
    public Set<String> getAirportsIataCodes() {
        return repository.listAllIataCodes();
    }

    @Override
    public AirportData getAirportData(String iata) {
        return repository.findById(iata);
    }
}
