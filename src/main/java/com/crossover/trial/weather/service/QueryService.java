package com.crossover.trial.weather.service;

import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.dto.PingData;

import java.util.List;

public interface QueryService {

    /**
     * Calculates and returns some statistics about the applicaiton
     * */
    PingData ping();

    /**
     * Returns list of atmospheric information for certain radius around given airport.
     * */
    List<AtmosphericInformation> weather(String iata, double radius);
}
