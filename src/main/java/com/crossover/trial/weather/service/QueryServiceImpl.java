package com.crossover.trial.weather.service;

import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.dto.PingData;
import com.crossover.trial.weather.repository.AirportRepository;
import com.crossover.trial.weather.repository.AtmosphericInformationRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Math.*;


@Slf4j
public class QueryServiceImpl implements QueryService {

    /**
     * earth radius in KM
     */
    static final double EARTH_RADIUS = 6372.8;

    @Inject
    AtmosphericInformationRepository atmosphericInformationRepository;
    @Inject
    AirportRepository airportRepository;
    @Inject
    StatisticsService statisticsService;


    @Override
    public PingData ping() {
        Instant previousDay = Instant.now().minus(1, ChronoUnit.DAYS);
        final long datasize = atmosphericInformationRepository.countAllNonEmptyWhereLastUpdateGreater(previousDay);
        final Map<String, Double> airportsRequestFrequency = statisticsService.getAirportsRequestFrequency();
        final int[] radiusRequestFrequency = statisticsService.getRadiusRequestFrequency();

        return new PingData(datasize,
                airportsRequestFrequency,
                radiusRequestFrequency);
    }

    @Override
    public List<AtmosphericInformation> weather(String iata, double radius) {
        statisticsService.updateRequestFrequency(iata, radius);

        AirportData airport = airportRepository.findById(iata);
        if (airport == null) {
            throw new IllegalArgumentException("not found aiport with iata " + iata);
        }
        if (radius <= 0) {
            //no filtration of empty weather information
            return Collections.singletonList(airport.getInformation());
        } else {
            return airportRepository.findAll().stream()
                    .filter(otherAirport -> calculateDistance(airport, otherAirport) <= radius)
                    .map(AirportData::getInformation)
                    .filter(AtmosphericInformation::isNotEmpty).collect(Collectors.toList());
        }
    }

    /**
     * Haversine distance between two airports.
     * <p>
     * https://en.wikipedia.org/wiki/Haversine_formula,
     * https://wikimedia.org/api/rest_v1/media/math/render/svg/d9bdd722860b46cf459e01fe0108fa1f45f13fd6
     * <pre>
     *                                 _____________________________________________________________________
     *                              /    2 / lat2 - lat1 \       2 / lon2 - lon1 \
     *   2*EARTH_RADIUS * arcsin   /  sin (---------------) + sin ( ------------- ) * cos(lat1) * cos(lat2)
     *                           \/        \      2      /         \      2      /
     * </pre>
     *
     * @param ad1 airport 1
     * @param ad2 airport 2
     * @return the distance in KM
     */
    static double calculateDistance(AirportData ad1, AirportData ad2) {
        final double lat1 = toRadians(ad1.getLatitude());
        final double lat2 = toRadians(ad2.getLatitude());

        final double lon1 = toRadians(ad1.getLongitude());
        final double lon2 = toRadians(ad2.getLongitude());

        double deltaLat = lat2 - lat1;
        double deltaLon = lon2 - lon1;
        double a = (sin(deltaLat / 2) * sin(deltaLat / 2)) + (sin(deltaLon / 2) * sin(deltaLon / 2)) * cos(lat1) * cos(lat2);
        double c = 2 * asin(sqrt(a));
        return EARTH_RADIUS * c;
    }
}
