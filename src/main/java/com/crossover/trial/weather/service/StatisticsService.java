package com.crossover.trial.weather.service;

import java.util.Map;

public interface StatisticsService {

    /**
     * Calculates and returns airports request frequency.
     * <p>
     * Keys of the returned map represent iata codes that were requested with "/query/weather/{iata}/{radius}" requests.
     * Value is a frequency - double value equal to (num of req for iata code)/(total requests).
     *
     * @return map containing iata codes in key and frequency of requests for that code as value
     */
    Map<String, Double> getAirportsRequestFrequency();

    /**
     * Calculates and return radius request frequency as array (histogram). Each cell/idx represents number of requests with radius in 10 km range.
     * For example array [0,1,4] means that application has got 0 requests for radius 0-10 km, 1 request with radius between 10 and 20 km
     * and 4 requests for range 20-30 kilometers.
     *
     * @return array containing number of request
     */
    int[] getRadiusRequestFrequency();

    /**
     * Records information about how often requests are made
     *
     * @param iata   an iata code
     * @param radius query radius
     */
    void updateRequestFrequency(String iata, Double radius);
}
