package com.crossover.trial.weather.service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class StatisticsServiceImpl implements StatisticsService {
    /**
     * number of kilometers that represent each cell of array that is returned from {@link StatisticsService#getRadiusRequestFrequency()}
     */
    private static int NORMALIZATION_COEFFICIENT = 10;

    /**
     * Internal performance counter to better understand most requested information, this map can be improved but
     * for now provides the basis for future performance optimizations. Due to the stateless deployment architecture
     * we don't want to write this to disk, but will pull it off using a REST request and aggregate with other
     * performance metrics {@link QueryService#ping()}
     */
    private final Map<String, AtomicInteger> requestFrequency = new ConcurrentHashMap<>();

    private final CopyOnWriteArrayList<AtomicInteger> radiusFreqStats =
            new CopyOnWriteArrayList<>(createArrayOfAtomicIntegers(1));

    private final AtomicLong totalRequests = new AtomicLong();

    private void ensureCapacity(int requiredSize) {
        if (radiusFreqStats.size() < requiredSize) {
            radiusFreqStats.addAll(createArrayOfAtomicIntegers(requiredSize - radiusFreqStats.size()));
        }
    }

    /**
     * Creates list of provided size, filled with instances of AtomicInteger with 0 value. Not synchronized list.
     */
    private List<AtomicInteger> createArrayOfAtomicIntegers(int size) {
        ArrayList<AtomicInteger> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(new AtomicInteger());
        }
        return list;
    }

    @Override
    public Map<String, Double> getAirportsRequestFrequency() {
        // we need this synchronization because in #updateRequestFrequency we update 2 fields - totalRequests and requestFrequency
        synchronized (requestFrequency) {
            return calculateFrequency(requestFrequency, totalRequests);
        }
    }

    @Override
    public int[] getRadiusRequestFrequency() {
        return radiusFreqStats.stream().mapToInt(ai -> {
            if (ai == null)
                return 0;
            return ai.get();
        }).toArray();
    }

    private int calculateIndex(Double radius) {
        return radius.intValue() / NORMALIZATION_COEFFICIENT;
    }

    private <T> Map<T, Double> calculateFrequency(Map<T, ? extends Number> requestNumberMap, AtomicLong total) {
        if (requestNumberMap.isEmpty()) {
            return Collections.emptyMap();
        } else {
            final Map<T, Double> freqMap = new HashMap<>();
            requestNumberMap.forEach((obj, val) -> freqMap.put(obj, val.doubleValue() / total.get()));
            return freqMap;
        }
    }


    @Override
    public void updateRequestFrequency(String iata, Double radius) {

        //update airports stats
        synchronized (requestFrequency) {
            totalRequests.incrementAndGet();
            requestFrequency.computeIfAbsent(iata, s -> new AtomicInteger(0)).incrementAndGet();
        }

        // update statistic only for positive radius
        if (radius > 0) {
            // update radius stats
            final int idx = calculateIndex(radius);
            ensureCapacity(idx + 1);
            radiusFreqStats.get(idx).incrementAndGet();
        }

    }
}
