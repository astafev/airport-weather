package com.crossover.trial.weather.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.ZoneOffset;

public class ZoneOffsetSerializationUtils {

    private ZoneOffsetSerializationUtils() {
        throw new UnsupportedOperationException();
    }

    public static class ZoneOffsetSerializer extends JsonSerializer<ZoneOffset> {
        @Override
        public void serialize(ZoneOffset value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeObject(value.getTotalSeconds());
        }
    }

    public static class ZoneOffsetDeserializer extends JsonDeserializer<ZoneOffset> {
        @Override
        public ZoneOffset deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            return ZoneOffset.ofTotalSeconds(p.getIntValue());
        }
    }
}
