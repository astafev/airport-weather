package com.crossover.trial.weather;

import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.DST;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class AirportLoaderTest {

    public static class ParseAirportTest {
        @Test
        public void oneAirportStansted() throws IOException {
            final String testString = "10,\"Stansted\",\"London\",\"United Kingdom\",\"STN\",\"EGSS\",51.885,0.235,348,0,\"E\"";
            final List<AirportData> dataList = AirportLoader.parseAirportData(new StringReader(testString));
            assertEquals(dataList.toString(), 1, dataList.size());
            AirportData airportData = dataList.get(0);
            assertEquals("Stansted", airportData.getName());
            assertEquals("London", airportData.getCity());
            assertEquals("United Kingdom", airportData.getCountry());
            assertEquals("STN", airportData.getIata());
            assertEquals("EGSS", airportData.getIcao());
            assertEquals(51.885, airportData.getLatitude(), Double.MIN_VALUE);
            assertEquals(0.235, airportData.getLongitude(), Double.MIN_VALUE);
            assertEquals(348, airportData.getAltitude(), Double.MIN_VALUE);
            assertEquals(ZoneOffset.UTC, airportData.getTimezone());
            assertEquals(DST.E, airportData.getDst());
        }

        @Test
        public void templateFile() throws IOException {
            try (InputStream airportsTemplate = this.getClass().getClassLoader().getResourceAsStream("airports.dat")) {
                final List<AirportData> dataList =
                        AirportLoader.parseAirportData(new InputStreamReader(airportsTemplate));
                assertEquals(dataList.toString(), 10, dataList.size());
                assertEquals("General Edward Lawrence Logan Intl", dataList.get(0).getName());
                assertEquals("Newark Liberty Intl", dataList.get(1).getName());
                assertEquals("John F Kennedy Intl", dataList.get(2).getName());
                assertEquals("City", dataList.get(3).getName());
                assertEquals("La Guardia", dataList.get(4).getName());
                assertEquals("Heathrow", dataList.get(5).getName());
                assertEquals("Luton", dataList.get(6).getName());
                assertEquals("Liverpool", dataList.get(7).getName());
                assertEquals("Morristown Municipal Airport", dataList.get(8).getName());
                assertEquals("Stansted", dataList.get(9).getName());
            }
        }
    }

    @RunWith(Parameterized.class)
    public static class ZoneOffsetCalculationTest {
        private final ZoneOffset expected;
        private final String value;

        public ZoneOffsetCalculationTest(ZoneOffset expected, String value) {
            this.expected = expected;
            this.value = value;
        }

        @Test
        public void test() {
            final ZoneOffset result = AirportLoader.zoneOffsetFromStringOffset(value);
            assertEquals(expected, result);
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {
                            ZoneOffset.ofHoursMinutes(5, 30), "5.5"
                    },
                    {
                            ZoneOffset.ofHours(5), "5"
                    },
                    {
                            ZoneOffset.ofHours(5), "5.0"
                    },
                    {
                            ZoneOffset.ofHours(0), "0"
                    },
                    {
                            ZoneOffset.ofHoursMinutes(-2, -15), "-2.25"
                    },
                    {
                            ZoneOffset.ofHoursMinutesSeconds(0, -7, -30), "-0.125"
                    },
            });
        }
    }
}