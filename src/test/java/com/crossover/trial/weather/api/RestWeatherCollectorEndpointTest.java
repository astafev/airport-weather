package com.crossover.trial.weather.api;

import com.crossover.trial.weather.WeatherException;
import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.domain.DataPointType;
import com.crossover.trial.weather.service.CollectorService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RestWeatherCollectorEndpointTest {
    public static final String CORRECT_DATA_POINT_JSON = "{\"mean\":1.0,\"first\":2,\"second\":3,\"third\":4,\"count\":5}";
    private RestWeatherCollectorEndpoint endpoint;
    private CollectorService service;

    @Before
    public void setUp() {
        endpoint = new RestWeatherCollectorEndpoint();
        service = mock(CollectorService.class);
        endpoint.service = service;
    }

    @Test
    public void testPing() {
        Response response = endpoint.ping();
        assertEquals(200, response.getStatus());
        assertEquals("ready", response.getEntity());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateWeather() throws WeatherException {
        final Response response = endpoint.updateWeather("IATA", "humidity",
                CORRECT_DATA_POINT_JSON);
        assertEquals(200, response.getStatus());
        verify(service).addDataPoint("IATA", DataPointType.HUMIDITY,
                DataPoint.builder()
                        .mean(1.0)
                        .first(2)
                        .median(3)
                        .last(4)
                        .count(5)
                        .build());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateWeatherWrongType() throws WeatherException {
        final Response response = endpoint.updateWeather("IATA", "__wrong point type__",
                CORRECT_DATA_POINT_JSON);
        assertEquals(Response.Status.Family.CLIENT_ERROR, response.getStatusInfo().getFamily());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateWeatherWrongDataPointFormat() throws WeatherException {
        final Response response = endpoint.updateWeather("IATA", "humidity",
                "first=1");
        assertEquals(Response.Status.Family.CLIENT_ERROR, response.getStatusInfo().getFamily());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateWeatherInternalException() throws WeatherException {
        doThrow(new WeatherException()).when(service).addDataPoint(anyString(),
                any(DataPointType.class),
                any(DataPoint.class));
        final Response response = endpoint.updateWeather("IATA", "humidity",
                CORRECT_DATA_POINT_JSON);
        assertEquals(Response.Status.Family.SERVER_ERROR, response.getStatusInfo().getFamily());
    }

    @Test
    public void testGetAirportsEmpty() {
        final Response response = endpoint.getAirports();
        final Set<Object> expected = Collections.emptySet();
        assertSucessResponse(response, expected);
    }

    private void assertSucessResponse(Response response, Object expected) {
        assertEquals(200, response.getStatus());
        assertEquals(expected, response.getEntity());
    }

    @Test
    public void testGetAirport() {
        final AirportData data = new AirportData("IATA", 0.0, 1.0);
        when(service.getAirportData("IATA")).thenReturn(data);
        assertSucessResponse(endpoint.getAirport("IATA"), data);
    }

    @Test
    public void testGetAirport404() {
        assertEquals(404, endpoint.getAirport("IATA").getStatus());
    }

    @Test
    public void testAddAirport3Values() {
        final Response response = endpoint.addAirport("IATA", "1", "2.0");
        assertEquals(200, response.getStatus());
        verify(service).addAirport("IATA", 1, 2.0);
    }

    @Test
    public void testAddAirport3ValuesParseError() {
        final Response response = endpoint.addAirport("IATA", "abc", "2.0");
        assertEquals(Response.Status.Family.CLIENT_ERROR, response.getStatusInfo().getFamily());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testAddAirportWithBodyError() throws IOException {
        final Response response = endpoint.addAirport("");
        assertEquals(String.valueOf(response.getEntity()),
                Response.Status.Family.CLIENT_ERROR, response.getStatusInfo().getFamily());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testAddAirportWithBodyEmptyData() throws IOException {
        final Response response = endpoint.addAirport("{}");
        assertEquals(String.valueOf(response.getEntity()),
                Response.Status.Family.CLIENT_ERROR, response.getStatusInfo().getFamily());
    }

    @Test
    public void testAddAirportWithBody() throws IOException {
        final Response response = endpoint.addAirport("{\"iata\":\"IATA\",\"latitude\":1.0,\"longitude\":2.0}");
        assertEquals(String.valueOf(response.getEntity()),
                Response.Status.Family.SUCCESSFUL, response.getStatusInfo().getFamily());
        verify(service).addAirport(new AirportData("IATA", 1.0, 2.0));
    }

    @Test
    public void testDeleteAirportNotFound() throws IOException {
        final Response response = endpoint.deleteAirport("IATA");
        assertEquals(404, response.getStatus());
        verify(service).deleteAirport("IATA");
    }

    @Test
    public void testDeleteAirportSuccess() throws IOException {
        when(service.deleteAirport("IATA")).thenReturn(new AirportData());
        final Response response = endpoint.deleteAirport("IATA");
        assertEquals(200, response.getStatus());
    }

}