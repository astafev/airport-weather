package com.crossover.trial.weather.api;

import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.dto.PingData;
import com.crossover.trial.weather.service.QueryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class RestWeatherQueryEndpointImplTest {
    private RestWeatherQueryEndpointImpl endpoint;
    private QueryService service;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() {
        endpoint = new RestWeatherQueryEndpointImpl();
        service = mock(QueryService.class);
        endpoint.queryService = service;
    }

    @Test
    public void testPing() throws IOException {
        //prepare
        final PingData initData = new PingData(10L, Collections.singletonMap("IATA", 1.0),
                new int[]{
                        0, 1, 2
                });
        when(service.ping()).thenReturn(initData);
        final String pingResponse = endpoint.ping();
        //invoke
        PingData pingResult = mapper.readValue(pingResponse, PingData.class);

        assertEquals(initData, pingResult);
    }

    @Test
    public void testPingFormat() throws IOException {
        // prepare
        final PingData initData = new PingData(10L, Collections.singletonMap("IATA", 1.0),
                new int[]{
                        0, 1, 2
                });
        when(service.ping()).thenReturn(initData);
        final String pingResponse = endpoint.ping();

        // invoke
        Map<?, ?> pingResult = mapper.readValue(pingResponse, Map.class);

        assertTrue(pingResponse, pingResult.containsKey("datasize"));
        assertTrue(pingResponse, pingResult.containsKey("iata_freq"));
        assertTrue(pingResponse, pingResult.get("iata_freq") instanceof Map);
        assertTrue(pingResponse, pingResult.containsKey("radius_freq"));
        assertTrue(pingResponse, pingResult.get("radius_freq") instanceof List);
    }

    @Test
    public void testWeather() {
        // prepare data
        final AtmosphericInformation ai1 = new AtmosphericInformation();
        final AtmosphericInformation ai2 = new AtmosphericInformation();

        ai1.setHumidity(DataPoint.builder().count(1).first(10).build());
        ai1.setCloudCover(DataPoint.builder().count(10).first(10).median(10).build());
        ai2.setWind(DataPoint.builder().count(1).first(10).build());

        final List<AtmosphericInformation> value = Arrays.asList(ai1, ai2);
        when(service.weather("IATA", 1.0)).thenReturn(value);

        //invoke
        final Response iata = endpoint.weather("IATA", "1");

        assertEquals(value, iata.getEntity());
    }

    @Test
    public void testWeatherIllegalFormat() {
        //invoke
        final Response iata = endpoint.weather("IATA", "abc");

        assertEquals(Response.Status.Family.CLIENT_ERROR,
                iata.getStatusInfo().getFamily());
    }
}