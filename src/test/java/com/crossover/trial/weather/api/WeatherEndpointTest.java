package com.crossover.trial.weather.api;

import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.dto.PingData;
import com.crossover.trial.weather.service.QueryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * As project was enhanced with dependency injection, this test can't be used as it is.
 * But it's kept as a reference for manual testing.
 */
@Ignore
public class WeatherEndpointTest {

    private RestWeatherQueryEndpointImpl _query;

    private WeatherCollectorEndpoint _update = new RestWeatherCollectorEndpoint();

    private ObjectMapper jsonMapper = new ObjectMapper();

    private DataPoint _dp;

    @Before
    public void setUp() throws Exception {
        _query = new RestWeatherQueryEndpointImpl();
        _query.queryService = mock(QueryService.class);
//        when(_query.weather(""))

//        RestWeatherQueryEndpointImpl.init();
        _dp = DataPoint.builder()
                .count(10).first(10).median(20).last(30).mean(22).build();
        _update.updateWeather("BOS", "wind", jsonMapper.writeValueAsString(_dp));
        _query.weather("BOS", "0").getEntity();
    }

    @Test
    public void testPing() throws Exception {
        String ping = _query.ping();
        final PingData pingResult = jsonMapper.readValue(ping, PingData.class);
        assertEquals(1L, pingResult.getDatasize());
        assertEquals(5, pingResult.getIataFreq().size());
    }

    @Test
    public void testGet() throws Exception {
        List<AtmosphericInformation> ais = (List<AtmosphericInformation>) _query.weather("BOS", "0").getEntity();
        assertEquals(ais.get(0).getWind(), _dp);
    }

    @Test
    public void testGetNearby() throws Exception {
        // check datasize response
        _update.updateWeather("JFK", "wind", jsonMapper.writeValueAsString(_dp));
        _dp.setMean(40);
        _update.updateWeather("EWR", "wind", jsonMapper.writeValueAsString(_dp));
        _dp.setMean(30);
        _update.updateWeather("LGA", "wind", jsonMapper.writeValueAsString(_dp));

        List<AtmosphericInformation> ais = (List<AtmosphericInformation>) _query.weather("JFK", "200").getEntity();
        assertEquals(3, ais.size());
    }

    @Test
    public void testUpdate() throws Exception {

        DataPoint windDp = DataPoint.builder()
                .count(10).first(10).median(20).last(30).mean(22).build();
        _update.updateWeather("BOS", "wind", jsonMapper.writeValueAsString(windDp));
        _query.weather("BOS", "0").getEntity();

        String ping = _query.ping();
        assertEquals(1L, jsonMapper.readValue(ping, PingData.class).getDatasize());

        DataPoint cloudCoverDp = DataPoint.builder()
                .count(4).first(10).median(60).last(100).mean(50).build();
        _update.updateWeather("BOS", "cloudcover", jsonMapper.writeValueAsString(cloudCoverDp));

        List<AtmosphericInformation> ais = (List<AtmosphericInformation>) _query.weather("BOS", "0").getEntity();
        assertEquals(ais.get(0).getWind(), windDp);
        assertEquals(ais.get(0).getCloudCover(), cloudCoverDp);
    }

}