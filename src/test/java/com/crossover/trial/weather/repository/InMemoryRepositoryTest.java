package com.crossover.trial.weather.repository;

import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.domain.DataPoint;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

import static org.junit.Assert.*;

public class InMemoryRepositoryTest {
    private static final String IATA = "IAT";
    private InMemoryRepository repository;

    @Before
    public void setUp() {
        repository = new InMemoryRepository();
    }

    @Test
    public void testAddAirport() {
        final AirportData data = new AirportData(IATA, 1.0, 0.0);
        repository.addAirport(data);
        assertSame(data, repository.findById(IATA));
        assertEquals(Collections.singletonList(data), repository.findAll());
        assertEquals(Collections.singleton(IATA), repository.listAllIataCodes());
    }

    @Test
    public void testDeleteAirport() {
        repository.addAirport(new AirportData(IATA, 1.0, 0.0));
        repository.delete(IATA);
        assertNull(repository.findById(IATA));
        assertEquals(Collections.emptyList(), repository.findAll());
        assertEquals(Collections.emptySet(), repository.listAllIataCodes());
    }

    @Test
    public void testCountAllNonEmpty() {
        final AirportData data = new AirportData(IATA, 1.0, 0.0);
        repository.addAirport(data);
        // 0 because information is empty
        assertEquals(0, repository.countAllNonEmptyWhereLastUpdateGreater(Instant.now().minus(1, ChronoUnit.HOURS)));
        final AtmosphericInformation information = new AtmosphericInformation();
        data.setInformation(information);
        information.setHumidity(DataPoint.builder().count(1).build());
        assertEquals(1, repository.countAllNonEmptyWhereLastUpdateGreater(Instant.now().minus(1, ChronoUnit.HOURS)));
        assertEquals(0, repository.countAllNonEmptyWhereLastUpdateGreater(Instant.now()));
    }
}