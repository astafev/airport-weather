package com.crossover.trial.weather.service;

import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.domain.DataPointType;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class CollectorServiceImplUpdateAtmosphericInformationTest {


    private static final Instant DEFAULT_LAST_UPDATE_TIME = Instant.EPOCH;

    private static final double[] ZERO_HUNDRED_CORRECT_TEST = {0, Double.MIN_VALUE, 1, 50, 99.9999};

    private static final double[] ZERO_HUNDRED_WRONG_TEST = {-1, -0.0001, 100, Double.MAX_VALUE, -Double.MAX_VALUE, Double.NaN};

    private final DataPointType pointType;

    private final double[] correctValues;

    private final double[] wrongValues;

    private final Method getter;

    //stateless for this test
    private final CollectorServiceImpl endpoint = new CollectorServiceImpl();

    private AtmosphericInformation information;

    public CollectorServiceImplUpdateAtmosphericInformationTest(String pointType, double[] correctValues, double[] wrongValues) throws NoSuchMethodException {
        this.pointType = DataPointType.valueOf(pointType.toUpperCase());
        this.correctValues = correctValues;
        this.wrongValues = wrongValues;
            getter = AtmosphericInformation.class.getMethod("get" + StringUtils.capitalize(pointType));
    }

    @Before
    public void initAtmosphericInformation() {
        information = new AtmosphericInformation();
        information.setLastUpdateTime(DEFAULT_LAST_UPDATE_TIME);
    }

    /**
     * for each
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {
                        "Wind",
                        new double[]{0, 1, 2, 500_000, Double.MAX_VALUE, Double.MIN_VALUE},
                        new double[]{-1, Double.NaN, -100_000, -Double.MIN_VALUE}
                },
                {
                        "Temperature",
                        new double[]{-50, 0, -Double.MIN_VALUE, Double.MIN_VALUE, 50, 99.999},
                        new double[]{Double.MAX_VALUE, 100, -Double.MAX_VALUE, -50.001}
                },
                {
                        "Humidity",
                        ZERO_HUNDRED_CORRECT_TEST,
                        ZERO_HUNDRED_WRONG_TEST
                },
                {
                        "Pressure",
                        new double[]{650, 700, 799.999, 650.001},
                        new double[]{0, 100, Double.MIN_VALUE, Double.MAX_VALUE, Double.NaN, 800}
                },
                {
                        "CloudCover",
                        ZERO_HUNDRED_CORRECT_TEST,
                        ZERO_HUNDRED_WRONG_TEST
                },
                {
                        "Precipitation",
                        ZERO_HUNDRED_CORRECT_TEST,
                        ZERO_HUNDRED_WRONG_TEST
                },
        });
    }

    @Test
    public void testWrong() throws Exception {
        for (double value : wrongValues) {
            initAtmosphericInformation();
            testWrong(value);
        }
    }

    private void testWrong(double value) throws InvocationTargetException, IllegalAccessException {
        final DataPoint db = DataPoint.builder().mean(value).build();

        endpoint.updateAtmosphericInformation(information, pointType, db);

        final String message = value + " was accepted (shouldn't have).";
        assertNull(message, getter.invoke(information));
        assertEquals(message, DEFAULT_LAST_UPDATE_TIME, information.getLastUpdateTime());
    }

    @Test
    public void testSuccess() throws Exception {
        for (double value : correctValues) {
            initAtmosphericInformation();
            testSuccess(value);
        }
    }

    private void testSuccess(double value) throws InvocationTargetException, IllegalAccessException {
        final DataPoint db = DataPoint.builder().mean(value).build();

        final Instant before = Instant.now();
        endpoint.updateAtmosphericInformation(information, pointType, db);
        final Instant after = Instant.now();

        final String message = value + " wasn't accepted (should have).";
        assertSame(message, getter.invoke(information), db);

        final Instant lastUpdateTime = information.getLastUpdateTime();
        assertTrue(message + " wrong update time: " + lastUpdateTime + "<" + before, !lastUpdateTime.isBefore(before));
        assertTrue(message + " wrong update time: " + lastUpdateTime + ">" + after, !lastUpdateTime.isAfter(after));
    }
}