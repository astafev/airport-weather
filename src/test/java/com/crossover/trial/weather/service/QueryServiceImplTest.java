package com.crossover.trial.weather.service;

import com.crossover.trial.weather.domain.AirportData;
import com.crossover.trial.weather.domain.AtmosphericInformation;
import com.crossover.trial.weather.domain.DataPoint;
import com.crossover.trial.weather.dto.PingData;
import com.crossover.trial.weather.repository.AirportRepository;
import com.crossover.trial.weather.repository.AtmosphericInformationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Instant;
import java.util.*;

import static com.crossover.trial.weather.service.QueryServiceImpl.EARTH_RADIUS;
import static java.lang.Math.*;
import static java.lang.Math.cos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Enclosed.class)
public class QueryServiceImplTest {

    public static final String IATA = "IATA";

    public static class PingTest {
        private QueryServiceImpl service;
        private AtmosphericInformationRepository atmosphericRepository;
        private StatisticsService statisticsService;

        @Before
        public void setUp() {
            service = new QueryServiceImpl();
            atmosphericRepository = mock(AtmosphericInformationRepository.class);
            service.atmosphericInformationRepository = atmosphericRepository;
            statisticsService = mock(StatisticsService.class);
            service.statisticsService = statisticsService;
        }

        @Test
        public void testPing() {
            when(atmosphericRepository.countAllNonEmptyWhereLastUpdateGreater(any(Instant.class)))
                    .thenReturn(10L);
            final Map<String, Double> airportsFreq = Collections.singletonMap("key", 10.0);
            when(statisticsService.getAirportsRequestFrequency()).thenReturn(airportsFreq);
            final int[] radiusHist = new int[0];
            when(statisticsService.getRadiusRequestFrequency()).thenReturn(radiusHist);
            final PingData ping = service.ping();

            assertEquals(10L, ping.getDatasize());
            assertSame(airportsFreq, ping.getIataFreq());
            assertSame(radiusHist, ping.getRadiusFreq());
        }
    }

    public static class WeatherTest {
        private QueryServiceImpl service;
        private StatisticsService statisticsService;
        private AirportRepository airportRepository;

        @Before
        public void setUp() {
            service = new QueryServiceImpl();
            airportRepository = mock(AirportRepository.class);
            service.airportRepository = airportRepository;
            statisticsService = mock(StatisticsService.class);
            service.statisticsService = statisticsService;
        }

        @Test
        public void testZeroRadiusReturnsOneAirport() {
            final AirportData data = new AirportData();
            when(airportRepository.findById(IATA)).thenReturn(data);
            final AtmosphericInformation ai = new AtmosphericInformation();
            data.setInformation(ai);
            //verify
            assertEquals(Collections.singletonList(ai), service.weather(IATA, 0));

            verify(statisticsService).updateRequestFrequency(IATA, 0.0);
        }

        @Test(expected = IllegalArgumentException.class)
        public void testZeroRadiusReturnsNotFound() {
            service.weather(IATA, 0);

            verify(statisticsService).updateRequestFrequency(IATA, 0.0);
        }

        @Test
        public void testZeroRadiusReturnsFewAirportsNotEmptyData() {
            final AirportData data1 = new AirportData(IATA, 1.0, 1.0);
            final AirportData data2 = new AirportData("UAT", 1.5, 1.0);
            when(airportRepository.findAll()).thenReturn(Arrays.asList(data1, data2));
            when(airportRepository.findById(IATA)).thenReturn(data1);
            final AtmosphericInformation ai1 = new AtmosphericInformation();
            ai1.setWind(DataPoint.builder().count(1).build());
            final AtmosphericInformation ai2 = new AtmosphericInformation();
            ai2.setHumidity(DataPoint.builder().count(1).build());
            data1.setInformation(ai1);
            data2.setInformation(ai2);
            //invoke
            assertEquals(Arrays.asList(ai1, ai2), service.weather(IATA, 100.0));
            verify(statisticsService).updateRequestFrequency(IATA, 100.0);
        }

        @Test
        public void testZeroRadiusReturnsFewAirportsEmptyData() {
            final AirportData data1 = new AirportData(IATA, 1.0, 1.0);
            final AirportData data2 = new AirportData("UAT", 1.5, 1.0);
            when(airportRepository.findAll()).thenReturn(Arrays.asList(data1, data2));
            when(airportRepository.findById(IATA)).thenReturn(data1);
            data1.setInformation(new AtmosphericInformation());
            data2.setInformation(new AtmosphericInformation());
            //invoke
            assertEquals(Collections.emptyList(), service.weather(IATA, 100.0));
            verify(statisticsService).updateRequestFrequency(IATA, 100.0);
        }


    }

    @RunWith(Parameterized.class)
    public static class CalculateDistanceTest {
        private static final double DELTA = 0.00001;

        private static Random random = new Random();

        private final AirportData data1;
        private final AirportData data2;
        private final double expectedResult;

        public CalculateDistanceTest(double latitude1, double longitude1,
                                     double latitude2, double longitude2,
                                     double expectedResult) {
            data1 = new AirportData();
            data1.setLatitude(latitude1);
            data1.setLongitude(longitude1);
            data2 = new AirportData();
            data2.setLatitude(latitude2);
            data2.setLongitude(longitude2);
            this.expectedResult = expectedResult;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    // lat1, lon1,
                    // lat2, lon2,
                    // result
                    {
                            0, 0, //data1
                            0, 0, //data2
                            0
                    },
                    {
                            90, 0, //data1
                            0, 0, //data2
                            EARTH_RADIUS * Math.PI / 2
                    },
                    {
                            90, 0, //data1
                            -90, 0, //data2
                            EARTH_RADIUS * Math.PI
                    },
                    {
                            0, 90,  //data1
                            0, -90, //data2
                            EARTH_RADIUS * Math.PI
                    },
                    {
                            45, 0, //data1
                            0, 0, //data2
                            EARTH_RADIUS * Math.PI / 4
                    },
                    {
                            90, 0, //data1
                            -90, 0, //data2
                            EARTH_RADIUS * Math.PI
                    },
                    {
                            0, 90, //data1
                            0, 0, //data2
                            EARTH_RADIUS * Math.PI / 2
                    },
            });
        }

        @Test
        public void compareWithExpectedResult() {
            assertEquals(expectedResult,
                    QueryServiceImpl.calculateDistance(data1, data2),
                    DELTA);
            assertEquals(expectedResult,
                    QueryServiceImpl.calculateDistance(data2, data1),
                    DELTA);
        }

        @Test
        public void testSymmetry() {
            assertEquals(QueryServiceImpl.calculateDistance(data2, data1),
                    QueryServiceImpl.calculateDistance(data1, data2),
                    DELTA);
        }

        @Test
        public void compareResultWithAnotherFormulaOnRandomValues() {
            final AirportData d1 = new AirportData();
            d1.setLongitude(random.nextDouble() % 180);
            d1.setLatitude(random.nextDouble() % 90);

            final AirportData d2 = new AirportData();
            d2.setLongitude(random.nextDouble() % 180);
            d2.setLatitude(random.nextDouble() % 90);

            assertEquals(
                    String.format("%1$,.5f, %2$,.5f - %3$,.5f, %4$,.5f", d1.getLatitude(), d1.getLongitude(),
                            d2.getLatitude(), d2.getLongitude()),
                    calculateDistance2(d1, d2),
                    QueryServiceImpl.calculateDistance(d1, d2),
                    DELTA);
            assertEquals(calculateDistance2(d2, d1),
                    QueryServiceImpl.calculateDistance(d2, d1),
                    DELTA);
        }

        @Test
        public void compareResultWithAnotherFormula() {
            assertEquals(calculateDistance2(data1, data2),
                    QueryServiceImpl.calculateDistance(data1, data2),
                    DELTA);
            assertEquals(calculateDistance2(data2, data1),
                    QueryServiceImpl.calculateDistance(data2, data1),
                    DELTA);
        }
    }


    /**
     * Calculates distance between two airports in kilometers. Similar to {@see QueryServiceImpl#calculateDistance} but with another formula.
     * <p>
     * <pre>
     *                           /                                                              \
     *    EARTH_RADIUS * arccos ( sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1-lon2) )
     *                           \                                                              /
     * </pre>
     *
     * @param ad1 airport 1
     * @param ad2 airport 2
     * @return the distance in KM
     */
    static double calculateDistance2(AirportData ad1, AirportData ad2) {
        final double lat1 = toRadians(ad1.getLatitude());
        final double lat2 = toRadians(ad2.getLatitude());

        final double lon1 = toRadians(ad1.getLongitude());
        final double lon2 = toRadians(ad2.getLongitude());

        return EARTH_RADIUS * acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1 - lon2));
    }
}