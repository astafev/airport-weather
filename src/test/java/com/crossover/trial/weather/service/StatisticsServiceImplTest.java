package com.crossover.trial.weather.service;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.*;

public class StatisticsServiceImplTest {
    private StatisticsService service;

    @Before
    public void setUp() {
        service = new StatisticsServiceImpl();
    }

    @Test
    public void testGetEmptyRadiusRequestFrequency() {
        final int[] radiusRequestFrequency = service.getRadiusRequestFrequency();
        assertNotNull(radiusRequestFrequency);
        final String message = Arrays.toString(radiusRequestFrequency);
        for (int i : radiusRequestFrequency) {
            assertEquals(message, 0, i);
        }
    }

    @Test
    public void testGetNonEmptyRadiusRequestFrequency() {
        // fill with data
        service.updateRequestFrequency("1", 1.0);
        service.updateRequestFrequency("2", 2.0);
        service.updateRequestFrequency("30", 30.0);
        service.updateRequestFrequency("20", 20.0);
        service.updateRequestFrequency("abc", 0.0);
        service.updateRequestFrequency("abc", 1.0);

        // verify
        verifyRadiusReqFreq(new int[]{
                4, 0, 1, 1
        });

        // check proper update once again
        service.updateRequestFrequency("abc", 11.0);

        verifyRadiusReqFreq(new int[]{
                4, 1, 1, 1
        });
    }

    private void verifyRadiusReqFreq(int[] expecteds) {
        int[] radiusRequestFrequency = service.getRadiusRequestFrequency();
        String message = Arrays.toString(radiusRequestFrequency);
        for (int i = 0; i < radiusRequestFrequency.length; i++) {
            if (i >= expecteds.length) {
                assertEquals(message + " " + i, 0, radiusRequestFrequency[i]);
            } else {
                assertEquals(message + " " + i, expecteds[i], radiusRequestFrequency[i]);
            }
        }
    }

    @Test
    public void testGetNonEmptyRadiusRequestFrequencyBigValue() {
        // fill with data
        service.updateRequestFrequency("1", 1.0);
        service.updateRequestFrequency("2", 12_000.0);

        // verify
        final int[] expecteds = new int[1201];
        expecteds[0] = 1;
        expecteds[1200] = 1;
        verifyRadiusReqFreq(expecteds);
    }

    @Test
    public void testGetEmptyAirportsRequestFrequency() {
        assertTrue(service.getAirportsRequestFrequency().toString(), service.getAirportsRequestFrequency().isEmpty());
    }

    @Test
    public void testGetAirportsRequestFrequency() {
        // fill with data
        service.updateRequestFrequency("1", 1.0);
        service.updateRequestFrequency("3", 1.0);
        service.updateRequestFrequency("abc", 0.0);
        service.updateRequestFrequency("abc", 1.0);
        service.updateRequestFrequency("3", 1.0);

        assertEquals(new HashMap<String, Double>(){{
            put("1", 0.2);
            put("3", 0.4);
            put("abc", 0.4);
        }}, service.getAirportsRequestFrequency());
    }
}